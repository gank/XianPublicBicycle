// 获取全局应用程序实例对象
const app = getApp()

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    movies: [],
    imgUrls: [
      'http://sjbz.fd.zol-img.com.cn/t_s320x510c/g5/M00/05/0D/ChMkJlmyURyIGmXYAACTJ_4cyvAAAgSQQJ2QWYAAJM_393.jpg',
      'http://sjbz.fd.zol-img.com.cn/t_s320x510c/g5/M00/05/0D/ChMkJ1myURyISrCsAABh1sK0BEwAAgSQQJ133gAAGHu366.jpg',
      'http://sjbz.fd.zol-img.com.cn/t_s320x510c/g5/M00/05/0D/ChMkJ1myURyIcWL0AAXIZtwWvIEAAgSQQJ3DjUABch-255.jpg'
    ],
    indicatorDots: false,
    autoplay: false,
    interval: 5000,
    duration: 1000,
    loading: true
  },

  getCache() {
    
  },

  handleStart() {
    // TODO: 访问历史的问题
    wx.switchTab({
      url: '../index/index'
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})